// HEADER
// jQuery(function() {
//     var nav = jQuery('#nav');
//
//     // メニューのtop座標を取得する
//     var offsetTop = nav.offset().top;
//
//     var floatMenu = function() {
//         // スクロール位置がメニューのtop座標を超えたら固定にする
//         if (jQuery(window).scrollTop() > offsetTop) {
//             nav.addClass('fixed');
//         } else {
//             nav.removeClass('fixed');
//         }
//     }
//     jQuery(window).scroll(floatMenu);
//     jQuery('body').bind('touchmove', floatMenu);
// });
// END


//iframe
$(document).ready(function(){
  $('iframe').load(function() {
    $(this).css('height', $(this).contents().find('body').height() + 'px');
  });
});


//Pragin headroom
$(document).ready(function() {
    var $win = $(window),
        $nav = $('nav'),
        navHeight = $nav.outerHeight(),
        startPos = 0;

    $win.on('load scroll', function() {
        var value = $(this).scrollTop();
        if ( value > startPos && value > navHeight ) {
            $nav.css('top', '-' + navHeight + 'px');
        } else {
            $nav.css('top', '0');
        }
        startPos = value;
    });
});


// pageTop
$(function() {
    var topBtn = $('#page-top');
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 800) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});

$(function() {
    var topBtn = $('#page-top-foot');
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 800) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});

//Skill List FLEX
$(function() {
  var emptyCells, i;

  $('.contBox').each(function() {
    emptyCells = [];
    for (i = 0; i < $(this).find('.cell').length; i++) {
      emptyCells.push($('<li>', {
        class: 'skill cell is-empty'
      }));
    }
    $(this).append(emptyCells);
  });
});
//END
